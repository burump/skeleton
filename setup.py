from setuptools import setup, find_packages
from skeleton import PACKAGE_NAME, VERSION


if __name__ == '__main__':
    setup(
        name=PACKAGE_NAME,
        version=VERSION,
        description='A domestic closet skeleton.',
        author='Kilgore Trout',
        author_email='k.trout@example.com',
        url='http://example.com/',
        license='MIT',

        packages=find_packages(),

        package_data={
            '': ['conf/*.conf']
        },

        entry_points={'console_scripts': [
            '%s-server = %s:main' % (PACKAGE_NAME, PACKAGE_NAME)]},

        install_requires=['Flask==0.10.1', 'Flask-Assets==0.8',
                          'tornado==3.1', 'yuicompressor==2.4.7']
    )
