(function ($) {

    var appView = Backbone.View.extend({
        template: JST.dashboard,
        className: 'skeleton',

        render: function() {
            this.$el.html(this.template({ name: this.options.name }));
            return this;
        }
    });

    $.fn.skeleton = function(options) {
        var app = new appView(options);
        this.html(app.render().el);
        return this;
    };
}(jQuery));
