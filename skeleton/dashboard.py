import logging
from flask import Blueprint, render_template

log = logging.getLogger(__name__)
dashboard = Blueprint('dashboard', __name__)


@dashboard.route('/')
def index():
    return render_template('layout.html')
