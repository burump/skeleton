import os
import sys
import ConfigParser
import logging

from pkg_resources import resource_filename
from flask import Flask
from flask.ext.assets import Environment, Bundle
from tornado import autoreload
from tornado.wsgi import WSGIContainer
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.options import define, options

PACKAGE_NAME = __name__
VERSION = '0.1.0'
MAX_CLOSE = 8192

log = logging.getLogger(__name__)


def parse_config():
    config = ConfigParser.ConfigParser()
    locations = [resource_filename(__name__, 'conf/%s.conf' % PACKAGE_NAME),
                 '/etc/%s/%s.conf' % (PACKAGE_NAME, PACKAGE_NAME),
                 os.path.expanduser('~/.%s.conf' % PACKAGE_NAME)]
    try:
        config.read(locations)
        return config
    except ConfigParser.Error:
        os._exit(os.EX_CONFIG)


def parse_args(config):
    define('daemonize', type=bool,
           default=config.getboolean(PACKAGE_NAME, 'daemonize'),
           help='daemonize the server')
    define('port', type=int,
           default=config.getint(PACKAGE_NAME, 'port'),
           help='server port number')
    define('debug', type=bool,
           default=config.getboolean(PACKAGE_NAME, 'debug'),
           help='enable the debug mode')
    options.parse_command_line()


def register_assets(app):
    assets = Environment(app)

    templates = Bundle(
        'templates/*.html', filters='jst')

    javascripts = Bundle(
        templates, 'js/skeleton.js',
        filters='yui_js', output='_packed.js')

    stylesheets = Bundle(
        'css/style.css',
        filters='yui_css', output='_packed.css')

    assets.register('js_all', javascripts)
    assets.register('css_all', stylesheets)


def create_app(config=None):
    app = Flask(__name__)

    if config is not None:
        app.config.update(config)

    register_assets(app)

    from skeleton.dashboard import dashboard
    app.register_blueprint(dashboard)

    return app


def daemonize():
    try:
        pid = os.fork()
    except OSError:
        os._exit(os.EX_OSERR)

    if pid == 0:
        os.setsid()

        try:
            pid = os.fork()
        except OSError:
            os._exit(os.EX_OSERR)

        if pid == 0:
            os.umask(0)
            os.chdir('/')

            if 'SC_OPEN_MAX' in os.sysconf_names:
                maxfd = os.sysconf('SC_OPEN_MAX')
            else:
                maxfd = MAX_CLOSE
            os.closerange(0, maxfd)

            os.open(os.devnull, os.O_RDWR)
            os.dup2(sys.stdin.fileno(), sys.stdout.fileno())
            os.dup2(sys.stdin.fileno(), sys.stderr.fileno())
        else:
            os._exit(os.EX_OK)
    else:
        os._exit(os.EX_OK)


def main():
    config = parse_config()
    parse_args(config)

    app = create_app()
    app.debug = options.debug

    log.info('Starting %s v%s on port %d', PACKAGE_NAME, VERSION, options.port)

    if options.daemonize:
        daemonize()

    http_server = HTTPServer(WSGIContainer(app))
    http_server.listen(options.port)
    ioloop = IOLoop.instance()
    if options.debug:
        autoreload.start(ioloop)
    ioloop.start()


if __name__ == '__main__':
    main()
